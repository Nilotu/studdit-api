const chai = require('chai');
const chaiHttp = require('chai-http');
const server = require('../src/app');
const assert = require('assert');
const  mongoose = require('mongoose');

const Comment = mongoose.model('comment');

chai.should();
chai.use(chaiHttp);

let createCommentId;
let threadId;

describe('Comment Controller', () => {

    before((done) => {
        chai.request(server)
            .post('/user')
            .send({username: "JestTest", password: "TestingJest"})
            .then(() => {
                chai.request(server)
                    .post('/thread')
                    .send({
                        username: 'JestTest',
                        title: 'Thread testing title',
                        content: 'This thread is made for testing'
                    })
                    .end((err, res) => {
                        threadId = res.body._id;
                        done();
                    })
            });
    });

    it('should post a comment', function (done) {
        Comment.countDocuments().then(count => {
            chai.request(server)
                .post('/comment/' + threadId)
                .send({username: "JestTest", content: "TestingContent"})
                .end((err, res) => {

                    createCommentId = res.body._id;

                    res.should.have.status(200);
                    Comment.countDocuments().then(newCount => {
                        assert(count + 1 === newCount);
                        done()
                    })
                })
            });
    });

    it('Should upvote a comment', (done) => {
        chai
        .request(server)
        .post('/comment/upvote/' + createCommentId)
        .send({
            username: 'JestTest'
        })
        .end((err, res) => {
            res.should.have.status(200);
            upComment = res.body;

            upComment.should.have.property('upvotes').that.is.a('array');
            assert(upComment.upvotes.length === 1);

            done();
        });
    });

    it('Should downvote a comment', (done) => {
        chai
        .request(server)
        .post('/comment/downvote/' + createCommentId)
        .send({
            username: 'JestTest'
        })
        .end((err, res) => {
            res.should.have.status(200);
            downComment = res.body;

            downComment.should.have.property('downvotes').that.is.a('array');
            assert(downComment.downvotes.length === 1);

            done();
        });
    });

    it('should delete a comment', function (done) {
        chai.request(server)
            .delete('/comment/' + createCommentId)
            .end((err, res) => {
                res.should.have.status(200)
                Comment.findOne({_id: createCommentId})
                    .then((comment) => {
                        assert(comment === null)
                        done();
                    })
            })
    });

    after(function (done) {
        chai.request(server)
            .delete('/thread/' + threadId)
            .send()
            .then(() => {
                chai.request(server)
                    .delete('/user')
                    .send({ username: 'JestTest', password: 'TestingJest' })
                    .end(() => {
                        done();
                    });
            });
    });
});