const chai = require('chai');
const chaiHttp = require('chai-http');
const server = require('../src/app');

chai.should()
chai.use(chaiHttp)

describe('Friend routes', () => {

    before((done) => {
        chai.request(server)
            .post('/user')
            .send({ username: 'TestUserOne', password: '9dRLqK' })
            .then(() => {
                chai.request(server)
                    .post('/user')
                    .send({ username: 'TestUserTwo', password: 'L5UbCk' })
                    .end((err, res) => {
                        done();
                    });
            });
    })

    it('Should add friendship', (done) => {
        chai.request(server)
            .post('/friend')
            .send({ userOne: 'TestUserOne', userTwo: 'TestUserTwo' })
            .end((err, res) => {
                res.should.have.status(200);

                done();
            })
    });

    it('Should 424 when frienduser is not existing', (done) => {
        chai.request(server)
            .post('/friend')
            .send({ userOne: 'notExistingUser', userTwo: 'notExistingUser2' })
            .end((err, res) => {
                res.should.have.status(424);

                done();
            })
    });

    it('Should delete friendship', (done) => {
        chai.request(server)
            .delete('/friend')
            .send({ userOne: 'TestUserOne', userTwo: 'TestUserTwo' })
            .end((err, res) => {
                res.should.have.status(200);

                done();
            })
    });

    after((done) => {
        chai.request(server)
            .delete('/user')
            .send({ username: 'TestUserOne', password: '9dRLqK' })
            .then(() => {
                chai.request(server)
                    .delete('/user')
                    .send({ username: 'TestUserTwo', password: 'L5UbCk' })
                    .end(() => {
                        done();
                    });
            });
    });
})