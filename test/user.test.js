const chai = require('chai');
const chaiHttp = require('chai-http');
const server = require('../src/app');
const assert = require('assert');
const mongoose = require('mongoose');

const User = mongoose.model('user');

chai.should();
chai.use(chaiHttp);

describe('User Controller', (done) => {

    before((done) => {
        chai.request(server)
            .post('/user')
            .send({ username: 'UserTesting', password: '2256386413' })
            .end((err, res) => {
                done();
            })
    });

    it('should add an user', function (done) {
        User.countDocuments().then(count => {
            chai.request(server)
                .post('/user')
                .send({username: "User1", password: "RandomPassword"})
                .end((err, res) => {
                    assert(res.should.have.status(200));
                    User.countDocuments().then(newCount => {
                        assert(count + 1 === newCount);
                        done();
                    });
                });
        });
    });

    it('should update an users password', function (done) {
        chai.request(server)
            .put('/user')
            .send({username: "UserTesting", password: "2256386413", newPassword: "newPassword"})
            .end(() => {
                User.findOne({username: "UserTesting"})
                    .then(user => {
                        assert(user.password === "newPassword")
                        done();
                    });
            });
    });

    it('should delete a user', function () {
        chai.request(server)
            .delete('/user')
            .send({ username: 'User1', password: 'RandomPassword' })
            .end(() =>{
                User.findOne({name: "User1"})
                    .then((user) => {
                        assert(user === null)
                        done();
                    });
            });
    });

    after((done) => {
        chai.request(server)
            .delete('/user')
            .send({ username: 'UserTesting', password: 'newPassword' })
            .end(() => {
                done()
            })
    })
});