const chai = require('chai');
const chaiHttp = require('chai-http');
const server = require('../src/app');
const assert = require('assert');

chai.should()
chai.use(chaiHttp)

let putThread;
let createThread;

describe('Thread routes', () => {

    before((done) => {
        chai.request(server)
            .post('/user')
            .send({ username: 'TestUserThread', password: '9dRLqK' })
            .then(() => {
                chai.request(server)
                    .post('/thread')
                    .send({
                        username: 'TestUserThread',
                        title: 'Thread testing title',
                        content: 'This thread is made for testing'
                    })
                    .end((err, res) => {
                        putThread = res.body._id;
                        chai.request(server)
                            .post('/user')
                            .send({username: "TestingTim", password: "TestingNilo"})
                            .then(() => {
                                chai.request(server)
                                    .post('/friend')
                                    .send({userOne: "TestingTim", userTwo: "TestUserThread"})
                                    .then(() => {
                                        done();
                                    })
                            })
                    });
            });
    })

    it('Should create a new thread', (done) => {
        chai
            .request(server)
            .post('/thread')
            .send({
                username: 'TestUserThread',
                title: 'Create thread test',
                content: 'This thread is made for the create test'
            })
            .end((err, res) => {
                res.should.have.status(200);
                testThread = res.body;

                testThread.should.have.property('_id');
                testThread.should.have.property('user');
                testThread.should.have.property('title');
                testThread.should.have.property('content');

                createThread = testThread._id;

                done();
            });
    });

    it('Should update thread content', (done) => {
        const newContent = 'The content of this thread has changed';
        chai
            .request(server)
            .put('/thread/' + putThread)
            .send({content: newContent})
            .end((err, res) => {
                res.should.have.status(200);
                res.body.should.have.property('content').which.equals(newContent);
                done();
            });
    });

    it('Should get thread by id', (done) => {
        chai
            .request(server)
            .get('/thread/' + putThread)
            .end((err, res) => {
                res.should.have.status(200);
                retThread = res.body;

                retThread.should.have.property('title');
                retThread.should.have.property('username');
                retThread.should.have.property('content');
                retThread.should.have.property('upvotes');
                retThread.should.have.property('downvotes');
                retThread.should.have.property('comments').that.is.a('array');
                done();
            });
    });

    it('Should get all threads', (done) => {
        chai
            .request(server)
            .get('/thread')
            .end((err, res) => {
                res.should.have.status(200);
                retThread = res.body[0];

                retThread.should.have.property('_id')
                retThread.should.have.property('title');
                retThread.should.have.property('username');
                retThread.should.have.property('content');
                retThread.should.have.property('upvotes');
                retThread.should.have.property('downvotes');
                done();
            });
    });

    it('should get all threads from friends', function (done) {
        chai.request(server)
            .post('/thread/friends')
            .send({username: "TestingTim", friendLength: 1})
            .end((err, res) => {
                res.should.have.status(200)
                const friendThread = res.body[0];
                friendThread.should.have.property('_id')
                friendThread.should.have.property('title');
                friendThread.should.have.property('username');
                friendThread.should.have.property('content');
                friendThread.should.have.property('upvotes');
                friendThread.should.have.property('downvotes');

                done();
            })
    });

    it('Should upvote a thread', (done) => {
        chai
        .request(server)
        .post('/thread/upvote/' + putThread)
        .send({
            username: 'TestUserThread'
        })
        .end((err, res) => {
            res.should.have.status(200);
            upThread = res.body;

            upThread.should.have.property('upvotes').that.is.a('array');
            assert(upThread.upvotes.length === 1);

            done();
        });
    });

    it('Should downvote a thread', (done) => {
        chai
        .request(server)
        .post('/thread/downvote/' + putThread)
        .send({
            username: 'TestUserThread'
        })
        .end((err, res) => {
            res.should.have.status(200);
            downThread = res.body;

            downThread.should.have.property('downvotes').that.is.a('array');
            assert(downThread.downvotes.length === 1);

            done();
        });
    });

    it('Should delete thread', (done) => {
        chai.request(server)
            .delete('/thread/' + createThread)
            .end((err, res) => {
                res.should.have.status(200);
                res.body.should.have.property('_id');
                done();
            })
    });

    after((done) => {
        chai.request(server)
            .delete('/thread/' + putThread)
            .send()
            .then(() => {
                chai.request(server)
                    .delete('/user')
                    .send({ username: 'TestUserThread', password: '9dRLqK' })
                    .end(() => {
                        chai.request(server)
                            .delete('/user')
                            .send({username: "TestingTim", password: "TestingNilo"})
                            .then(() => {
                                chai.request(server)
                                    .delete('/friend')
                                    .send({userOne: "TestingTim", userTwo: "TestUserThread"})
                                    .then(() => {
                                        done();
                                    })
                            });
                    });
            });
    })
});