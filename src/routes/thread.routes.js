const express = require('express');
const router = express.Router();
const threadController = require('../controllers/thread.controller');


router.get('/', threadController.getAllThreads);
router.get('/:id', threadController.getThreadById);
router.post('/friends', threadController.getThreadsFromFriends);
router.post('/', threadController.makeThread);
router.post('/upvote/:id', threadController.upvoteThread);
router.post('/downvote/:id', threadController.downThread);
router.put('/:id', threadController.changeThread);
router.delete('/:id', threadController.deleteThread);


module.exports = router;