const express = require('express');
const router = express.Router();
const friendController = require('../controllers/friend.controller');

router.post('/', friendController.makeFriend);
router.delete('/', friendController.deleteFriend)

module.exports = router;