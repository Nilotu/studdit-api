const express = require('express');
const router = express.Router();
const userController = require('../controllers/user.controller')

router.post('/', userController.postUser);
router.delete('/', userController.deleteUser);
router.put('/', userController.updateUserPassword);

module.exports = router;