const express = require('express');
const router = express.Router();
const commentController = require('../controllers/comment.controller');

router.post('/:id', commentController.postComment);
router.post('/upvote/:id', commentController.upvoteComment);
router.post('/downvote/:id', commentController.downComment);
router.delete('/:id', commentController.deleteComment);

module.exports = router;