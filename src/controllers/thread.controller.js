const Thread = require('../modals/thread');
const User = require('../modals/user');
const Comment = require('../modals/comment');
const Mongoose = require('mongoose');
const neo4j = require('neo4j-driver').v1;
const dbconfig = require('../dbconfig/config');

const driver = neo4j.driver(
    dbconfig.neo4jConnection,
    neo4j.auth.basic(
        dbconfig.neo4jUser,
        dbconfig.neo4jPassword));

module.exports = {
    makeThread: (req, res) => {
        const title = req.body.title;
        const content = req.body.content;

        User.findOne(
            { username: req.body.username },
            (err, user) => {
                if (err) {
                    return console.error(err);
                }
                if (user) {
                    const newThread = new Thread({
                        user: user._id,
                        title: title,
                        content: content
                    });
                    if (title && content) {
                        newThread.save(() => {
                            res.status(200).send(newThread);
                        });
                    } else {
                        res.status(422).send('Expected title and content');
                    }
                } else {
                    res.status(404);
                }
            });
    },
    changeThread: (req, res) => {
        Thread.findOne(
            { _id: req.params["id"] },
            (err, thread) => {
                if (err) {
                    return console.error(err);
                }
                if (thread) {
                    thread.content = req.body.content;
                    thread.save(() => {
                        res.status(200).send(thread);
                    });
                } else {
                    res.sendStatus(422);
                }
            });
    },
    deleteThread: (req, res) => {
        Thread.findOne({ _id: req.params["id"] })
            .populate("Comments")
            .exec((err, thread) => {
                if (err) {
                    return console.error(err);
                }
                if (thread) {
                    thread.remove(() => {
                        thread.comments.forEach(comment => {
                            Comment.remove({_id: comment});
                        });
                        res.status(200).send(thread);
                    });
                } else {
                    res.sendStatus(422);
                }
            });
    },
    getAllThreads: (req, res) => {
        Thread.aggregate([
            {
                $lookup: {
                    from: "users",
                    as: "user",
                    localField: "user",
                    foreignField: "_id"
                }
            },
            {
                "$project": {
                    "title": 1,
                    "username": { "$arrayElemAt": ["$user.username", 0] },
                    "content": 1,
                    "upvotes": { "$size": "$upvotes" },
                    "downvotes": { "$size": "$downvotes" },
                    "comments" : { "$size": "$comments" }
                }
            },
            {
                $sort: {
                     comments : -1
                }
            }
        ]).exec((err, threads) => {
            if (err) {
                return console.error(err);
            }
            if (threads) {
                res.status(200).send(threads);
            }
        })
    },
    getThreadById: (req, res) => {
        Thread.aggregate([
            {
                "$match": { "_id": Mongoose.Types.ObjectId(req.params["id"]) }
            },
            {
                $lookup: {
                    from: "users",
                    localField: "user",
                    foreignField: "_id",
                    as: "user"
                }
            },
            {
                "$graphLookup": {
                    "from": "comments",
                    "startWith": "$comments",
                    "connectFromField": "comments",
                    "connectToField": "_id",
                    "as": "children"
                }
            },
            {
                "$project": {
                    "title": 1,
                    "username": { "$arrayElemAt": ["$user.username", 0] },
                    "content": 1,
                    "upvotes": { "$size": "$upvotes" },
                    "downvotes": { "$size": "$downvotes" },
                    "comments": "$children"
                }
            },
        ]).exec((err, thread) => {
            if (err) {
                return console.error(err);
            }
            if (thread && thread.length !== 0) {
                User.populate(
                    thread,
                    [{ path: "user", select: "username" }, { path: "comments.user", select: "username" }],
                    (err, result) => {
                        if (err) {
                            return console.error(err);
                        }
                        result[0].comments.forEach(comment => {
                            comment.upvotes = comment.upvotes.length
                            comment.downvotes = comment.downvotes.length
                        });
                        res.status(200).send(result[0]);
                    });
            } else {
                res.sendStatus(424);
            }
        })
    },

    getThreadsFromFriends: (req, res) => {
        const userName = req.body.username;
        const session = driver.session();
        const friendLength = req.body.friendLength;

        User.findOne({username: userName}, (err, user) => {
            if (err) console.error(err);
            if (user) {
                session.run(`MATCH (u1:Person)-[f:friend*1..${friendLength}]-(n) WHERE u1.name = $userName RETURN  n`, {userName: userName})
                    .then((friends) => {
                        // if (err) console.error(err);
                        if (friends) {
                            let persons = [];
                            friends.records.forEach(person => {
                                persons.push(person._fields[0].properties.name);

                            });
                            User.find({username: persons},{_id:1}, (err, result) => {
                                if (err) console.error(err);
                                if (result) {
                                    let ids = [];
                                    result.forEach(id => {
                                        ids.push(id._id);
                                    });
                                    Thread.aggregate([
                                        {
                                            "$match": {
                                                "user": {"$in": ids}
                                            }
                                        },
                                        {
                                            $lookup: {
                                                from: "users",
                                                as: "user",
                                                localField: "user",
                                                foreignField: "_id"
                                            }
                                        },
                                        {
                                            "$project": {
                                                "title": 1,
                                                "username": { "$arrayElemAt": ["$user.username", 0] },
                                                "content": 1,
                                                "upvotes": { "$size": "$upvotes" },
                                                "downvotes": { "$size": "$downvotes" },
                                            }
                                        },
                                    ]).exec((err, threads) => {
                                        if (err) {
                                            return console.error(err);
                                        }
                                        if (threads) {
                                            res.status(200).send(threads);
                                        }
                                    })
                                }
                            })
                        } else {
                            res.sendStatus(204);
                        }
                })
            } else {
                res.sendStatus(204);
            }
        })
    },

    upvoteThread: (req, res) => {
        User.findOne(
            { username: req.body.username },
            (err, user) => {
                if (err) {
                    return console.error(err);
                }
                if (user) {
                    Thread.findOne(
                        { _id: req.params["id"] },
                        (err, thread) => {
                            if (err) {
                                return console.error(err);
                            }
                            if (thread) {
                                if (!(thread.upvotes.includes(user._id))) {
                                    thread.upvotes.push(user._id);
                                    thread.downvotes.remove(user._id);
                                    thread.save(() => {
                                        res.status(200).send(thread);
                                    });
                                } else {
                                    res.sendStatus(405);
                                }
                            } else {
                                res.sendStatus(422);
                            }
                        });
                } else {
                    res.sendStatus(404);
                }
            });
    },
    downThread: (req, res) => {
        User.findOne(
            { username: req.body.username },
            (err, user) => {
                if (err) {
                    return console.error(err);
                }
                if (user) {
                    Thread.findOne(
                        { _id: req.params["id"] },
                        (err, thread) => {
                            if (err) {
                                return console.error(err);
                            }
                            if (thread) {
                                if (!(thread.downvotes.includes(user._id))) {
                                    thread.downvotes.push(user._id);
                                    thread.upvotes.remove(user._id);
                                    thread.save(() => {
                                        res.status(200).send(thread);
                                    });
                                } else {
                                    res.sendStatus(405);
                                }
                            } else {
                                res.sendStatus(422);
                            }
                        });
                } else {
                    res.sendStatus(404);
                }
            });
    }
};