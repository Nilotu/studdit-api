const User = require('../modals/user');

module.exports = {
    postUser (req, res, next) {
        const userProps = req.body;

        User.create(userProps)
            .then(user => res.status(200).send(user))
            .catch(next)
    },

    updateUserPassword (req, res, next) {
        const userName = req.body.username;
        const userPassword = req.body.password;
        const newPassword = req.body.newPassword;

        User.findOne({username: userName}, (err, user) => {
            if (err) return console.log(err);
            if(user) {
                if (user.password === userPassword) {
                    user.password = newPassword;
                    // user.set({password: newPassword});
                    user.save(() => {
                        res.status(200).send(user)
                    })
                } else {
                    res.status(401).send({message: "Password is incorrect", code: 401})
                }
            } else {
                res.send(404);
            }
        }).catch(next);
    },

    deleteUser(req, res, next) {
        const userName = req.body.username;
        const userPassword = req.body.password;

        User.findOne({username: userName})
            .then((user) => {
                if (user.password === userPassword) {
                    user.remove(user, () =>{
                        res.status(200).send(user)
                    })
                } else {
                    res.status(401).send({message: "Password is incorrect", code: 401})
                }
            })
            .catch(next)
    }
};