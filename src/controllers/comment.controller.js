const Comment = require('../modals/comment');
const Thread = require('../modals/thread');
const User = require('../modals/user');

module.exports = {
    postComment (req, res) {
        const id = req.params.id;
        const username = req.body.username;
        const content = req.body.content;

        User.findOne({username: username}, (err, user) => {
            if (err) console.log(err);
            if (user) {
                Thread.findOne({_id: id }, (err, thread) => {
                    if (err) console.log(err);
                    if (thread) {
                        const newComment = new Comment({
                            user: user._id,
                            content: content
                        });
                        newComment.save(() => {
                            thread.comments.push(newComment);
                            thread.save(() => {
                                res.status(200).send(newComment)
                            })
                        })
                    } else {
                        Comment.findOne({_id: id}, (err, comment) => {
                            if (err) console.log(err);
                            if (comment) {
                                const newComment = new Comment({
                                    user: user._id,
                                    content: content
                                });
                                newComment.save(() => {
                                    comment.comments.push(newComment);
                                    comment.save(() => {
                                        res.status(200).send(newComment)
                                    })
                                })
                            } else {
                                res.status(404);
                            }
                        })
                    }
                })
            } else {
                res.status(404);
            }
        })
    },

    deleteComment (req, res) {
        const id = req.params.id;

        Comment.findOne({_id: id}).populate('Comments').exec((err, comment) => {
            if (err) console.log(err);
            if (comment) {
                comment.remove(() => {
                    comment.comments.forEach(comm => {
                        Comment.remove({_id: comm}, () => {
                        });
                    });
                    Thread.findOne({ comments: { $in: [id ] } }, (err, thread) => {
                        if (err) console.log(err);
                        if (thread) {
                            thread.comments.remove(id);
                            thread.save();
                        } else {
                            Comment.findOne({comments: {$in: [id] } }, (err, comment) => {
                                if (err) console.log(err);
                                if (comment) {
                                    comment.comments.remove(id);
                                    comment.save();
                                }
                            });
                        }
                    });
                    res.status(200).send(comment);
                });
            } else {
                res.sendStatus(422);
            }
        });
    },
    upvoteComment: (req, res) => {
        User.findOne(
            { username: req.body.username },
            (err, user) => {
                if (err) {
                    return console.error(err);
                }
                if (user) {
                    Comment.findOne(
                        { _id: req.params["id"] },
                        (err, comment) => {
                            if (err) {
                                return console.error(err);
                            }
                            if (comment) {
                                if (!(comment.upvotes.includes(user._id))) {
                                    comment.upvotes.push(user._id);
                                    comment.downvotes.remove(user._id);
                                    comment.save(() => {
                                        res.status(200).send(comment);
                                    });
                                } else {
                                    res.sendStatus(405);
                                }
                            } else {
                                res.sendStatus(422);
                            }
                        });
                } else {
                    res.sendStatus(404);
                }
            });
    },
    downComment: (req, res) => {
        User.findOne(
            { username: req.body.username },
            (err, user) => {
                if (err) {
                    return console.error(err);
                }
                if (user) {
                    Comment.findOne(
                        { _id: req.params["id"] },
                        (err, comment) => {
                            if (err) {
                                return console.error(err);
                            }
                            if (comment) {
                                if (!(comment.downvotes.includes(user._id))) {
                                    comment.downvotes.push(user._id);
                                    comment.upvotes.remove(user._id);
                                    comment.save(() => {
                                        res.status(200).send(comment);
                                    });
                                } else {
                                    res.sendStatus(405);
                                }
                            } else {
                                res.sendStatus(422);
                            }
                        });
                } else {
                    res.sendStatus(404);
                }
            });
    }
};