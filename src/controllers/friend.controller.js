const User = require('../modals/user');
const neo4j = require('neo4j-driver').v1;
const dbconfig = require('../dbconfig/config');

const driver = neo4j.driver(
    dbconfig.neo4jConnection, 
    neo4j.auth.basic(
        dbconfig.neo4jUser, 
        dbconfig.neo4jPassword));

module.exports = {
    makeFriend: (req, res) => {
        const session = driver.session();

        userOne = req.body.userOne;
        userTwo = req.body.userTwo;

        User.find({username: userOne}).countDocuments((err, count) => {
            if(count !== 0){
                User.find({username: userTwo}).countDocuments((err, count) => {
                    if(count !== 0){
                        session.run(`MATCH (p:Person {name:$userOne}) return p`, {userOne: userOne})
                            .then((user1) => {
                                if (user1.records.length === 0){
                                    session.run('CREATE (p:Person {name: $name}) RETURN p', {name: userOne});
                                }
                            })
                            .then(() => {
                                session.run('MATCH (p:Person { name: $userTwo }) return p', {userTwo: userTwo})
                                .then((user2) => {
                                    if (user2.records.length === 0){
                                        session.run('CREATE (p:Person {name: $name}) RETURN p', {name: userTwo});
                                    }
                                })
                                .then(() => {
                                    session.run('MATCH (:Person {name: $userOne})-[r:friend]-(:Person {name: $userTwo}) return r', {userOne: userOne, userTwo: userTwo})
                                    .then((result) => {
                                        if(result.records.length === 0){
                                            session.run(`
                                            MATCH (a:Person), (b:Person)
                                            WHERE a.name = $userOne and b.name = $userTwo
                                            CREATE (a)-[r:friend]->(b)`, 
                                            {userOne: userOne, userTwo: userTwo})
                                            .then(() => {
                                                session.close();
                                            })
                                        }
                                    })
                                })
                            })
                            .then(() => {
                                res.status(200).send();
                            });
                    } else {
                        res.status(424).send();
                    }
                });
            } else {
                res.status(424).send();
            }
            session.close();
        });
    },
    deleteFriend: (req, res) => {
        const session = driver.session();

        userOne = req.body.userOne;
        userTwo = req.body.userTwo;

        session.run('MATCH (:Person {name: $userOne})-[r:friend]-(:Person {name: $userTwo}) DELETE r', {userOne: userOne, userTwo: userTwo})
        .then(() => {
            session.close();
        }).then(() => {
            res.status(200).send();
        });
    }
};