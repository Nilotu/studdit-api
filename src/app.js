const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const dbconfig = require('./dbconfig/config')

app.use(bodyParser.json());

app.on('databaseConnected', function () {
    const port = process.env.PORT || 3000;

    app.listen(port, () => {
        console.log(`Server is listening on port ${port}`)
    });
});

mongoose.connect(dbconfig.mongodbConnection || 'mongodb://localhost:27017/studdit', { useNewUrlParser: true, useUnifiedTopology: true })
    .then(() => {
        console.log('MongoDB connection established');

        app.emit('databaseConnected');
    })
    .catch(err => {
        console.log('MongoDB connection failed')
        console.log(err)
    })
const userRoutes = require('./routes/user.routes');
const commentRoutes = require('./routes/comment.routes');
const threadRoutes = require('./routes/thread.routes');
const friendRoutes = require('./routes/friend.routes');

app.use('/user', userRoutes);
app.use('/comment', commentRoutes);
app.use('/thread', threadRoutes);
app.use('/friend', friendRoutes);


app.use('/', (req, res) => {
    res.redirect('https://gitlab.com/Shroom/studdit-api');
});

app.use('*', function (req, res, next) {
    console.log('This endpoint does not exist');
    let message = {
        error: "This endpoint does not exist"
    };
    next(message)
});

app.use((err, req, res, next) => {
    console.log('Catch-all error handler was called.');
    console.log(err);

    res.status(404).json(err).end()
});

module.exports = app;