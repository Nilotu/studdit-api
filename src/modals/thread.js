const Mongoose = require('mongoose');
const Schema = Mongoose.Schema;
const CommentSchema = require('./comment');

const ThreadSchema = new Schema({
    user: {
        type: Schema.Types.ObjectId,
        required: [true, 'A users needs to be attached to a thread'],
        ref: 'user'
    },
    title: {
        type: String,
        required: [true, 'A thread needs a title'],
        
    },
    content: {
        type: String,
        required: [true, 'A thread needs content']
    },
    upvotes: [{
        type: Schema.Types.ObjectId,
        ref: 'user',
        default: []
    }],
    downvotes: [{
        type: Schema.Types.ObjectId,
        ref: 'user',
        default: []
    }],
    comments: [{
        type: Schema.Types.ObjectId,
        ref: 'comment',
        default: []
    }],
    versionKey: false
});

const Thread = Mongoose.model('thread', ThreadSchema)

ThreadSchema.virtual('karma').get(function() {
    return (this.upvotes.length - this.downvotes.length);
});

module.exports = Thread;