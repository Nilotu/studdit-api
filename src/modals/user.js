const Mongoose = require('mongoose');
const Schema = Mongoose.Schema;

const UserSchema = new Schema({
    username: {
        type: String,
        required: [true, 'A user needs to have a name'],
        unique: [true, 'A username must be unique'],
    },
    password:{
        type: String,
        required: [true, 'A user needs to have a password'],
        validate: {
            validator: (password) => password.length > 5,
            message: 'Password must be atleast 6 characters long'
          }
    },
    versionKey: false
});

const User = Mongoose.model('user', UserSchema);

module.exports = User;
