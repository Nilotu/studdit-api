const Mongoose = require('mongoose');
const Schema = Mongoose.Schema;

const CommentSchema = new Schema({
    user: {
        type: Schema.Types.ObjectId,
        required: [true, 'A users needs to be attached to a comment'],
        ref: 'user'
    },
    content: {
        type: String,
        required: [true, 'A comment needs content']
    },
    upvotes: [{
        type: Schema.Types.ObjectId,
        ref: 'user',
        default: []
    }],
    downvotes: [{
        type: Schema.Types.ObjectId,
        ref: 'user',
        default: []
    }],
    comments: [{
        type: Schema.Types.ObjectId,
        ref: 'comment',
        default: []
    }],
    versionKey: false
});

const Comment = Mongoose.model('comment', CommentSchema);

CommentSchema.virtual('karma').get(function() {
    return (this.upvotes.length - this.downvotes.length);
});

module.exports = Comment;